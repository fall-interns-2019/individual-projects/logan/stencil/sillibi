import {Component, Element, h, Prop} from '@stencil/core';
import $ from "jquery";
import {AppRoot} from "../app-root/app-root";

@Component({
  tag: 'sillibi-assignments-create',
  styleUrl: 'sillibi-assignments-create.css'
})
export class SillibiAssignmentsCreate {

  @Element() el: HTMLElement;

  @Prop() modalController: HTMLIonModalControllerElement;
  @Prop() courseId: number;

  private form: HTMLFormElement;
  private debounce = false;

  async onCancelClicked() {
    await this.modalController.dismiss();
  }

  async onAddAssignmentClicked() {
    if(this.debounce || !this.form.reportValidity()) {
      return;
    }

    this.debounce = true;

    const name = $('#input-name').val().toString();
    const due_date = $('#input-due-date').val().toString();
    const description = $('#input-description').val().toString();
    const possible_points = $('#input-possible-points').val().toString();

    const button = $('#button-add-assignment')[0] as HTMLIonButtonElement;
    button.disabled = true;

    const result = await AppRoot.getAssignmentHelper().create({
      course_id: this.courseId,
      name, due_date, description, possible_points
    })
      .catch((err) => {
        AppRoot.showNotification(err, 'danger');
      });

    this.debounce = false;
    button.disabled = false;

    if(!result) return;

    return this.modalController.dismiss(result.data);
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar>
          <ion-title>Add Assignment</ion-title>
          <ion-buttons slot="primary">
            <ion-button color="tertiary" onClick={() => this.onCancelClicked()}>Cancel</ion-button>
          </ion-buttons>
        </ion-toolbar>
      </ion-header>,
      <ion-content color="light">
        <ion-card style={{'--background': '#ffffff'}}>
          <ion-card-content class="ion-no-padding">
            <form ref={(el) => this.form = el}>
              <ion-list lines="none">
                <ion-item lines="inset" class="ion-form-text ion-padding-horizontal">
                  <ion-label color="dark" position="stacked">Assignment name</ion-label>
                  <ion-input id="input-name" required type="text" color="medium"
                             placeholder="Exam 01"/>
                </ion-item>
                <ion-item lines="inset" class="ion-form-text ion-padding-horizontal">
                  <ion-label color="dark" position="stacked">Due date</ion-label>
                  <ion-datetime id="input-due-date" display-format="MM/DD/YYYY"
                                placeholder="mm/dd/yy"
                                min={(new Date().getFullYear() - 1).toString()}
                                max={(new Date().getFullYear() + 1).toString()} />
                </ion-item>
                <ion-item lines="inset" class="ion-form-text ion-padding-horizontal">
                  <ion-label color="dark" position="stacked">Description</ion-label>
                  <ion-input id="input-description" required type="text" color="medium"
                             placeholder="Write a twelve page essay"/>
                </ion-item>
                <ion-item lines="inset" class="ion-form-text ion-padding-horizontal">
                  <ion-label color="dark" position="stacked">Possible points</ion-label>
                  <ion-input id="input-possible-points" required type="number" min="0"
                             max="1000" color="medium" placeholder="50"/>
                </ion-item>
              </ion-list>
            </form>

            <ion-button id="button-add-assignment" class="ion-padding center-horiz half-width"
                        color="secondary" onClick={() => this.onAddAssignmentClicked()}>
              Create Assignment
            </ion-button>
          </ion-card-content>
        </ion-card>
      </ion-content>
    ]
  }
}
