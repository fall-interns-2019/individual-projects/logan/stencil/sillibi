import {Component, Element, h, Prop} from '@stencil/core';
import {ApiHelper} from "../../helpers/apiHelper";
import {CourseHelper} from "../../helpers/courseHelper";
import {SyllabusHelper} from "../../helpers/syllabusHelper";
import {AssignmentHelper} from "../../helpers/assignmentHelper";

@Component({
  tag: 'app-root',
  styleUrl: 'app-root.css'
})
export class AppRoot {

  @Element() el: HTMLElement;

  @Prop() ApiHelper: ApiHelper; // = new ApiHelper();
  @Prop() CourseHelper: CourseHelper; // = new CourseHelper();
  @Prop() SyllabusHelper: SyllabusHelper; // = new SyllabusHelper();
  @Prop() AssignmentHelper: AssignmentHelper; // = new AssignmentHelper();

  private static routes = [
    {
      props: {url: '/', component: 'sillibi-home-page'},
      access: 'default'
    },
    {
      props: {url: '/register', component: 'sillibi-register-page'},
      access: 'default'
    },
    {
      props: {url: '/login', component: 'sillibi-login-page'},
      access: 'default'
    },
    {
      props: {url: '/profile', component: 'sillibi-profile-page'},
      access: 'default_auth'
    },
    {
      props: {url: '/courses', component: 'sillibi-courses-page'},
      access: 'default_auth'
    },
    {
      props: {url: '/courses/*', component: 'sillibi-course-detail'},
      access: 'default_auth'
    },
    {
      props: {url: '/assignments', component: 'sillibi-assignments-page'},
      access: 'default_auth'
    },
    {
      props: {url: '/assignments/*', component: 'sillibi-assignment-detail'},
      access: 'default_auth'
    },
    {
      props: {url: '/test*'},
      access: 'default_auth'
    },
  ];

  constructor() {
    this.ApiHelper = new ApiHelper();
    this.CourseHelper = new CourseHelper();
    this.SyllabusHelper = new SyllabusHelper();
    this.AssignmentHelper = new AssignmentHelper();
  }

  static getApiHelper() {
    return document.querySelector('app-root').ApiHelper;
  }

  static getCourseHelper() {
    return document.querySelector('app-root').CourseHelper;
  }

  static getSyllabusHelper() {
    return document.querySelector('app-root').SyllabusHelper;
  }

  static getAssignmentHelper() {
    return document.querySelector('app-root').AssignmentHelper;
  }

  static async route(url: string) {
    document.querySelector('ion-router').push(url, 'root');
  }

  static async validateUser() {
    let user = await AppRoot.getApiHelper().validateUser()
      .catch((err) => {
        console.log('validateUser error:', err);
      });

    if (!user) return false;

    console.log('validateUser: user is valid:', AppRoot.getApiHelper().user);

    return user;
  }

  static async checkAccess(routePath) {
    let routeInfo = AppRoot.routes.find((route) => {
      return route.props.url.includes('*')
        ? routePath.match(route.props.url)
        : routePath === route.props.url;
    });
    if (!routeInfo) return false;

    let currentUser = await AppRoot.validateUser();
    let destination;

    if (typeof routeInfo.access === 'string') {
      if (routeInfo.access === 'default' && currentUser) {
        destination = '/profile';
      } else if (routeInfo.access === 'default_auth') {
        if (currentUser) {
          await AppRoot.getApiHelper().waitForUser();
        } else {
          destination = '/login';
        }
      }
    } else {
      // TODO: custom redirecting
    }

    return [!destination, destination] as any;
  }

  private static async onIonRouteWillChange(event) {
    const [access, redirect] = await AppRoot.checkAccess(event.detail.to);

    console.log('onIonRouteWillChange: trying (', event.detail.to, ') access(', access, ') redirect(', redirect, ')');

    if (!access) {
      console.log('onIonRouteWillChange: no access! will redirect!');
      window.location.href = redirect;
    }
  }

  static async showNotification(message, color?, waitForDismiss?) {
    const toast = await document.querySelector('ion-toast-controller').create({
      message, color,
      duration: 2000,
      position: 'middle',
    });

    await toast.present();

    if (waitForDismiss)
      await toast.onWillDismiss();
  }

  render() {
    return (
      <ion-app>
        <ion-toast-controller/>
        <ion-router useHash={false} onIonRouteWillChange={(evt) => AppRoot.onIonRouteWillChange(evt)}>
          <ion-route url="/" component="sillibi-home-page"/>
          <ion-route url="/register" component="sillibi-register-page"/>
          <ion-route url="/login" component="sillibi-login-page"/>
          <ion-route url="/profile" component="sillibi-profile-page"/>
          <ion-route url="/courses" component="sillibi-courses-page"/>
          <ion-route url="/courses/:courseId" component="sillibi-course-detail"/>
          <ion-route url="/courses/:courseId/syllabus" component="sillibi-syllabus-page"/>
          <ion-route url="/courses/:courseId/assignments" component="sillibi-assignments-page"/>
          <ion-route url="/assignments" component="sillibi-assignments-page"/>
          <ion-route url="/assignments/:assignmentId" component="sillibi-assignment-detail"/>

          { /* temp testing routes */}
          <ion-route url="/test1" component="sillibi-syllabus-uploads"/>
        </ion-router>
        <ion-nav/>
      </ion-app>
    );
  }
}
