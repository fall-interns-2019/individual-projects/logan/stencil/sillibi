import {Component, Prop, h, Element} from '@stencil/core';

@Component({
  tag: 'sillibi-tab-bar',
  styleUrl: 'sillibi-tab-bar.css'
})
export class SillibiTabBar {

  private tabs = {
    profile: {route: '/profile', icon: 'contact'},
    courses: {route: '/courses', icon: 'document'},
    assignments: {route: '/assignments', icon: 'list-box'},
    misc: {route: null, icon: 'more'}
  };

  @Element() el: HTMLElement;

  @Prop() tab: string;

  changeTab(route) {
    if(!route) return;

    document.querySelector('ion-router').push(route);
  }

  generateTabButtons() {
    return Object.keys(this.tabs).map((tabName) => {
      const tabData = this.tabs[tabName];
      return (
        <ion-tab-button onClick={() => this.changeTab(tabData.route)}>
          <ion-icon color={tabName === this.tab ? 'secondary' : ''} name={tabData.icon} />
        </ion-tab-button>
      )
    })
  }

  render() {
    return [
      <ion-tab-bar slot="bottom" color="primary" selectedTab={this.tab}>
        {this.generateTabButtons()}
      </ion-tab-bar>
    ]
  }
}
