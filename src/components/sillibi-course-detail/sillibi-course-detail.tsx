import {Component, Element, h, Prop, State} from '@stencil/core';
import {AppRoot} from "../app-root/app-root";
import $ from "jquery";

@Component({
  tag: 'sillibi-course-detail',
  styleUrl: 'sillibi-course-detail.css'
})
export class SillibiCourseDetail {

  @Element() el: HTMLElement;

  @Prop() courseId: number;

  @State() editMode = false;

  private course: any;
  private alertController: HTMLIonAlertControllerElement;
  private form: HTMLFormElement;
  private debounce = false;

  async componentWillLoad() {
    const {data} = await AppRoot.getCourseHelper().get(this.courseId);
    if(!data) return;

    this.course = data;
  }

  onEditButtonClick() {
    this.editMode = !this.editMode;
  }

  async onSaveButtonClicked() {
    if(this.debounce || !this.form.reportValidity()) {
      return;
    }

    this.debounce = true;

    const name = $('#input-course-name').val().toString();
    const course_number = $('#input-course-number').val().toString();
    const section_number = $('#input-section-number').val().toString();
    const term = $('#input-term').val().toString();
    const instructor = $('#input-instructor').val().toString();

    const button = $('#button-save')[0] as HTMLIonButtonElement;
    button.disabled = true;

    const result = await AppRoot.getCourseHelper().update(this.courseId, {
        name, course_number, section_number, term, instructor
      })
      .catch((err) => {
        AppRoot.showNotification(err, 'danger');
      });

    this.debounce = false;
    button.disabled = false;

    if(!result) return;

    this.editMode = false;
    // document.querySelector('sillibi-courses-page');
  }

  async onDeleteButtonClicked() {
    if(this.debounce) return;

    this.debounce = true;

    const alert = await this.alertController.create({
      header: 'Confirmation',
      message: 'Are you sure you want to <strong>delete</strong> this course?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Delete',
          role: 'delete'
        }
      ]
    });

    await alert.present();
    const result = await alert.onWillDismiss();

    if(result.role !== 'delete') {
      this.debounce = false;
      return;
    }

    const delete_result = await AppRoot.getCourseHelper().destroy(this.courseId)
      .catch((err) => {
        AppRoot.showNotification(err, 'danger');
      });

    this.debounce = false;

    if(!delete_result) return;

    await AppRoot.route('/courses');
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar>
          <ion-buttons slot="start">
            <ion-back-button defaultHref="/courses"/>
          </ion-buttons>
          <ion-title>{this.editMode ? 'Edit Course' : 'Course Information'}</ion-title>
          <ion-buttons slot="primary">
            <ion-button color="tertiary"
                        onClick={() => this.onEditButtonClick()}>
              {this.editMode ? 'Cancel' : 'Edit'}
            </ion-button>
          </ion-buttons>
        </ion-toolbar>
      </ion-header>,
      <ion-alert-controller ref={(el) => this.alertController = el} />,
      <ion-content color="light">
        <ion-card style={{'--background': '#ffffff'}}>
          <ion-card-content class="ion-no-padding">
            <form ref={(el) => this.form = el}>
              <ion-list lines="none">
                <ion-item lines={this.editMode ? "inset" : "none"} class="ion-form-text ion-padding-horizontal">
                  <ion-label color="medium" position="stacked">Course name</ion-label>
                  <ion-input id="input-course-name" required type="text" color="dark"
                             value={this.course.name} readonly={!this.editMode}/>
                </ion-item>
                <ion-item lines={this.editMode ? "inset" : "none"} class="ion-form-text ion-padding-horizontal">
                  <ion-label color="medium" position="stacked">Course number</ion-label>
                  <ion-input id="input-course-number" required type="text" color="dark"
                             value={this.course.course_number} readonly={!this.editMode}/>
                </ion-item>
                <ion-item lines={this.editMode ? "inset" : "none"} class="ion-form-text ion-padding-horizontal">
                  <ion-label color="medium" position="stacked">Section number</ion-label>
                  <ion-input id="input-section-number" required type="text" color="dark"
                             value={this.course.section_number} readonly={!this.editMode}/>
                </ion-item>
                <ion-item lines={this.editMode ? "inset" : "none"} class="ion-form-text ion-padding-horizontal">
                  <ion-label color="medium" position="stacked">Term</ion-label>
                  <ion-input id="input-term" required type="text" color="dark"
                             value={this.course.term} readonly={!this.editMode}/>
                </ion-item>
                <ion-item lines={this.editMode ? "inset" : "none"} class="ion-form-text ion-padding-horizontal">
                  <ion-label color="medium" position="stacked">Instructor</ion-label>
                  <ion-input id="input-instructor" required type="text" color="dark"
                             value={this.course.instructor} readonly={!this.editMode}/>
                </ion-item>

                {!this.editMode ?
                  [
                    <ion-item-divider/>,
                    <ion-item lines="full" class="ion-padding-horizontal">
                      <ion-icon name="document" slot="start" color="tertiary"/>
                      <ion-label>Syllabus uploads</ion-label>
                      <ion-note slot="end">{this.course.syllabus_count}</ion-note>
                    </ion-item>,
                    <ion-item class="ion-padding-horizontal">
                      <ion-icon name="list-box" slot="start" color="tertiary"/>
                      <ion-label>Assignments</ion-label>
                      <ion-note slot="end">{this.course.assignment_count}</ion-note>
                    </ion-item>
                  ] : null
                }
              </ion-list>
            </form>

            {this.editMode ?
              [
                <br/>,
                <ion-button id="button-save" class="center-horiz half-width" color="secondary"
                            onClick={() => this.onSaveButtonClicked()}>
                  Save Changes
                </ion-button>,
                <br/>,
                <ion-button id="button-delete" class="center-horiz half-width" color="medium" fill="clear"
                            onClick={() => this.onDeleteButtonClicked()}>
                  Delete Course
                </ion-button>,
                <br />
              ] : null
            }
          </ion-card-content>
        </ion-card>
      </ion-content>
    ]
  }
}
