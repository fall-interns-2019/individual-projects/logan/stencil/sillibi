import {Component, Element, h, Listen, State} from '@stencil/core';
import {AppRoot} from "../app-root/app-root";

@Component({
  tag: 'sillibi-courses-page',
  styleUrl: 'sillibi-courses-page.css'
})
export class SillibiCoursesPage {

  @Element() el: HTMLElement;

  @State() courses: any[] = [];

  @Listen('assignmentCreated')
  assignmentCreatedHandler(event: CustomEvent) {
    const index = this.courses.findIndex((course) => course.id === event.detail.course_id);
    const course = this.courses[index];
    course.assignment_count++;

    this.courses = [...this.courses, course];
  }

  private modalController: HTMLIonModalControllerElement;

  async componentWillRender() {
    await AppRoot.getApiHelper().waitForUser();

    const {data} = await AppRoot.getCourseHelper().requestAll();
    if(!data) return;

    this.courses = data.sort((a, b) => new Date(a.created_by) < new Date(b.created_by) ? 1 : -1);
  }

  async onNewCourseClicked() {
    const modal = await this.modalController.create({
      component: 'sillibi-courses-create',
      componentProps: {
        modalController: this.modalController
      }
    });

    await modal.present();

    const {data} = await modal.onWillDismiss();
    if(!data) return;

    this.courses = [...this.courses, data];
  }

  private generateCourseCards() {
    return this.courses.map((course) => {
      return <sillibi-course-card course={course} />
    })
  }

  private renderCourses = () => (
    <ion-grid>
      <ion-row align-items-center justify-content-around>
        <ion-col>
          <ion-list style={{'background': 'var(--ion-color-light)'}}>
            {this.generateCourseCards()}
          </ion-list>
        </ion-col>
      </ion-row>
      <ion-row>
        <ion-col>
          <ion-button class="center-horiz" fill="clear" shape="round" onClick={() => this.onNewCourseClicked()}>
            <img src="/assets/BTN_AddCourse.svg"/>
          </ion-button>
        </ion-col>
      </ion-row>
    </ion-grid>
  );

  private renderNoCourses = () => (
    <ion-grid>
      <ion-row style={{height: '70vh'}} align-items-center justify-content-around>
        <ion-col>
          <ion-button class="center-horiz" fill="clear" shape="round" onClick={() => this.onNewCourseClicked()}>
            <img style={{height: '65vh', width: '65vh'}} src="/assets/illo_AddCourse.svg"/>
          </ion-button>
        </ion-col>
      </ion-row>
      <ion-row>
        <ion-col>
          <ion-label class="ion-padding center-horiz" text-wrap>
            Add a course to upload a syllabus.
          </ion-label>
        </ion-col>
      </ion-row>
    </ion-grid>
  );

  render() {
    return [
      <ion-header>
        <ion-toolbar>
          <ion-title>My Courses</ion-title>
        </ion-toolbar>
      </ion-header>,
      <ion-modal-controller ref={(el) => this.modalController = el} />,
      <ion-content color="light">
        {this.courses.length > 0 ? this.renderCourses() : this.renderNoCourses()}
      </ion-content>,
      <sillibi-tab-bar tab="courses"/>
    ]
  }
}
