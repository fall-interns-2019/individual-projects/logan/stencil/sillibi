import {Component, h} from '@stencil/core';

@Component({
  tag: 'sillibi-walkthrough',
  styleUrl: 'sillibi-walkthrough.css'
})
export class SillibiWalkthrough {

  render() {
    return [
      <ion-content color="primary">
        <ion-slides style={{
          '--bullet-background-active': '#ffffff',
          '--bullet-background': 'var(--ion-color-step-200, #ffffff)',
          height: '100vh'}} pager={true}>
          <ion-slide>
            <img src="/assets/Sillibi-Logo.svg" alt="Sillibi Logo"/>
          </ion-slide>
          <ion-slide>

          </ion-slide>
          <ion-slide>

          </ion-slide>
          <ion-slide>

          </ion-slide>
        </ion-slides>
      </ion-content>
    ];
  }
}
