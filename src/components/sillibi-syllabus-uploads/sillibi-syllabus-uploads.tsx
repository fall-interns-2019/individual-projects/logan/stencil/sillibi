import {Component, h} from '@stencil/core';

@Component({
  tag: 'sillibi-syllabus-uploads',
  styleUrl: 'sillibi-syllabus-uploads.css'
})
export class SillibiSyllabusUploads {

  testingFlag = false;

  private renderPictures = () => [
    <ion-content color="light">
      <ion-list style={{'background': 'var(--ion-color-light)'}} class="ion-padding">
        <ion-card style={{'--background': '#ffffff', height: '50vh', width: '40vh'}} class="ion-padding center-horiz">
          <img class="center-horiz" src="/assets/Fake_Syllabus.png"/>
        </ion-card>
      </ion-list>
    </ion-content>,
    <ion-footer>
      <ion-button style={{width: '100%'}} class="ion-no-padding ion-no-margin"
                  color="secondary">Add Another Image
      </ion-button>
    </ion-footer>
  ];

  private renderNoPictures = () => (
    <ion-content color="light">
      <ion-grid>
        <ion-row style={{height: '75vh'}} align-items-center justify-content-around>
          <ion-col>
            <img style={{height: '50vh', width: '50vh'}} class="center-horiz" src="/assets/Illo-robot.svg"/>
          </ion-col>
        </ion-row>
        <ion-row>
          <ion-col>
            <ion-label class="ion-padding center-horiz" text-wrap>
              Upload your syllabus and one of our robot elves will input your assignments for you.
            </ion-label>
          </ion-col>
        </ion-row>
        <ion-row>
          <ion-col>
            <ion-button class="center-horiz" color="tertiary" fill="clear">Upload PDF</ion-button>
          </ion-col>
        </ion-row>
      </ion-grid>
    </ion-content>
  );

  render() {
    return [
      <ion-header>
        <ion-toolbar>
          <ion-buttons slot="start">
            <ion-back-button/>
          </ion-buttons>
          <ion-title>Syllabus Upload</ion-title>
        </ion-toolbar>
      </ion-header>,
      this.testingFlag ? this.renderPictures() : this.renderNoPictures(),
    ]
  }
}
