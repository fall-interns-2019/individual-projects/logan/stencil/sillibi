import {Component, Element, h} from '@stencil/core';
import $ from 'jquery';
import {AppRoot} from "../app-root/app-root";

@Component({
  tag: 'sillibi-register-page',
  styleUrl: 'sillibi-register-page.css'
})
export class SillibiRegisterPage {

  @Element() el: HTMLElement;

  private formInputs = {};
  private form: HTMLFormElement;
  private debounce = false;

  private async onCreateAccountClick() {
    if(this.debounce || !this.form.reportValidity()) {
      return;
    }

    this.debounce = true;

    let button = $('#button-create')[0] as HTMLIonButtonElement;
    button.disabled = true;

    let first_name = this.formInputs['input-first-name'];
    let last_name = this.formInputs['input-last-name'];
    let email = this.formInputs['input-email'];
    let password = this.formInputs['input-password'];

    let result = await AppRoot.getApiHelper().emailSignUp(
      {email, password, password_confirmation: password},
      {first_name, last_name}
      )
      .catch((err) => {
        AppRoot.showNotification(err, 'danger');
      });

    this.debounce = false;
    button.disabled = false;

    if(!result) return;

    console.log('result:', result);
    await AppRoot.route('/profile');
  }

  private async onKeyDown(event) {
    if(event.key === 'Enter')
      await this.onCreateAccountClick();
  }

  private handleFormInputChanged(event) {
    this.formInputs[event.target.id] = event.detail.value;
  }

  render() {
    return [
      <ion-content color="primary">
        <ion-grid>
          <ion-row style={{height: '100vh'}} align-items-center justify-content-around>
            <ion-col>
              <form ref={(el) => this.form = el}>
                <ion-list style={{'background': 'var(--ion-color-primary)'}} class="ion-padding">
                  <ion-item color="primary" lines="inset">
                    <ion-label color="secondary" position="stacked">First Name</ion-label>
                    <ion-input id="input-first-name" required type="text"
                               onIonChange={(evt) => this.handleFormInputChanged(evt)}/>
                  </ion-item>
                  <ion-item color="primary" lines="inset">
                    <ion-label color="secondary" position="stacked">Last Name</ion-label>
                    <ion-input id="input-last-name" required type="text"
                               onIonChange={(evt) => this.handleFormInputChanged(evt)}/>
                  </ion-item>
                  <ion-item color="primary" lines="inset">
                    <ion-input id="input-email" required type="email" placeholder="Email Address"
                               onIonChange={(evt) => this.handleFormInputChanged(evt)}/>
                  </ion-item>
                  <ion-item color="primary" lines="inset">
                    <ion-input id="input-password" required type="password" placeholder="Password"
                               onKeyDown={(evt) => this.onKeyDown(evt)}
                               onIonChange={(evt) => this.handleFormInputChanged(evt)}/>
                  </ion-item>
                </ion-list>
                <ion-button id="button-create" class="ion-padding center-horiz button-margin half-width"
                            color="secondary" onClick={() => this.onCreateAccountClick()}>
                  Create Account
                </ion-button>
                <ion-label class="ion-text-center center-horiz">Already have an account?</ion-label>
                <ion-button href="/login" style={{'display': 'table'}} class="center-horiz" fill="clear">
                  <ion-label color="tertiary">Sign In</ion-label>
                </ion-button>
              </form>
            </ion-col>
          </ion-row>
        </ion-grid>
      </ion-content>
    ]
  }
}
