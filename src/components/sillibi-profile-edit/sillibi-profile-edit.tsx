import {Component, Element, h, Prop, State} from '@stencil/core';
import {AppRoot} from "../app-root/app-root";
import $ from 'jquery';
import {getBase64FromFile} from "../../helpers/utils";

@Component({
  tag: 'sillibi-profile-edit',
  styleUrl: 'sillibi-profile-edit.css'
})
export class SillibiProfileEdit {

  @Element() el: HTMLElement;

  @Prop() modalController: HTMLIonModalControllerElement;

  @State() avatarBase64: string;

  private form: HTMLFormElement;
  private debounce = false;

  async componentWillRender() {
    await AppRoot.getApiHelper().waitForUser();
  }

  async onCancelClicked() {
    await this.modalController.dismiss();
  }

  async onSaveChangesClicked() {
    if (this.debounce || !this.form.reportValidity()) {
      return;
    }

    this.debounce = true;

    const first_name = $('#input-first-name').val().toString();
    const last_name = $('#input-last-name').val().toString();
    const email = $('#input-email').val().toString();
    const call_checked = $('#toggle-call').prop('checked');
    const email_checked = $('#toggle-email').prop('checked');

    const current_email = AppRoot.getApiHelper().user.data.email;

    const button = $('#button-save')[0] as HTMLIonButtonElement;
    button.disabled = true;

    const result = await AppRoot.getApiHelper().updateUser({
      first_name, last_name,
      avatar: this.avatarBase64 || undefined,
      email: email !== current_email ? email : undefined,
      uid: email !== current_email ? email : undefined,
      calls_enabled: call_checked,
      emails_enabled: email_checked,
    })
      .catch((err) => {
        AppRoot.showNotification(err, 'danger');
      });

    this.debounce = false;
    button.disabled = false;

    if (!result) return;

    return this.modalController.dismiss(result);
  }

  onAvatarClicked() {
    const input = this.el.querySelector('#input-avatar') as HTMLInputElement;
    input.click();
  }

  async onAvatarFileChanged(event) {
    if (!event.target || event.target.files.length === 0) return;

    const image = event.target.files[0] as File;

    if(!image.type.toLowerCase().startsWith('image')) {
      await AppRoot.showNotification(`Avatar must be an image`, 'danger');
      return;
    }

    const kb_size = image.size / 1000;
    const max_kb_size = 500;

    if (kb_size > max_kb_size) {
      await AppRoot.showNotification(`Avatar size must not exceed ${max_kb_size}kb`, 'danger');
      return;
    }

    const encoded = await getBase64FromFile(image);
    this.avatarBase64 = encoded.toString();
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar>
          <ion-title>Edit Profile</ion-title>
          <ion-buttons slot="primary">
            <ion-button color="tertiary" onClick={() => this.onCancelClicked()}>Cancel</ion-button>
          </ion-buttons>
        </ion-toolbar>
      </ion-header>,
      <ion-content color="light">
        <ion-card style={{'--background': '#ffffff'}}>
          <ion-card-content class="ion-no-padding">
            <div class="ion-padding-vertical">
              <ion-button class="center-horiz" color="dark" fill="clear" shape="round">
                <ion-avatar>
                  <input id="input-avatar" type="file" accept="image/*" hidden
                         onChange={(evt) => this.onAvatarFileChanged(evt)}/>
                  <img id="img-avatar" src={this.avatarBase64
                  || AppRoot.getApiHelper().user.data.avatar
                  || `https://ui-avatars.com/api/?name=
                            ${AppRoot.getApiHelper().user.data.first_name}
                            + ${AppRoot.getApiHelper().user.data.last_name}
                            &rounded=true&background=32134E&color=ffffff`}
                       onClick={() => this.onAvatarClicked()}/>
                </ion-avatar>
              </ion-button>
            </div>

            <form ref={(el) => this.form = el}>
              <ion-list lines="none">
                <ion-item lines="inset" class="ion-form-text ion-padding-horizontal">
                  <ion-label color="medium" position="stacked">First Name</ion-label>
                  <ion-input id="input-first-name" required type="text"
                             value={AppRoot.getApiHelper().user.data.first_name}/>
                </ion-item>
                <ion-item lines="inset" class="ion-form-text ion-padding-horizontal">
                  <ion-label color="medium" position="stacked">Last Name</ion-label>
                  <ion-input id="input-last-name" required type="text"
                             value={AppRoot.getApiHelper().user.data.last_name}/>
                </ion-item>
                <ion-item lines="inset" class="ion-form-text ion-padding-horizontal">
                  <ion-label color="medium" position="stacked">Email address</ion-label>
                  <ion-input id="input-email" required type="email"
                             value={AppRoot.getApiHelper().user.data.email}/>
                </ion-item>

                <ion-item-divider class="ion-padding-top ion-text-uppercase">
                  <ion-label class="ion-padding-horizontal">Communication Settings</ion-label>
                </ion-item-divider>

                <ion-item lines="full">
                  <ion-label slot="start" color="medium" class="ion-padding-horizontal">Call</ion-label>
                  <ion-toggle id="toggle-call" style={{
                    '--background-checked': 'var(--ion-color-success-shade)',
                    '--handle-background-checked': 'var(--ion-color-light)'
                  }} class="ion-padding-horizontal" slot="end"
                              checked={AppRoot.getApiHelper().user.data.calls_enabled}/>
                </ion-item>
                <ion-item lines="full">
                  <ion-label slot="start" color="medium" class="ion-padding-horizontal">E-Mail</ion-label>
                  <ion-toggle id="toggle-email" style={{
                    '--background-checked': 'var(--ion-color-success-shade)',
                    '--handle-background-checked': 'var(--ion-color-light)'
                  }} class="ion-padding-horizontal" slot="end"
                              checked={AppRoot.getApiHelper().user.data.emails_enabled}/>
                </ion-item>
              </ion-list>
            </form>

            <ion-button id="button-save" class="ion-padding center-horiz half-width" color="secondary"
                        onClick={() => this.onSaveChangesClicked()}>
              Save Changes
            </ion-button>
          </ion-card-content>
        </ion-card>
      </ion-content>
    ]
  }
}
