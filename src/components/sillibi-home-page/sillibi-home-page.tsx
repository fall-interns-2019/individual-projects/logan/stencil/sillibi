import {Component, h} from '@stencil/core';

@Component({
  tag: 'sillibi-home-page',
  styleUrl: 'sillibi-home-page.css'
})
export class SillibiHomePage {

  render() {
    return [
      <ion-content color="primary">
        <ion-grid>
          <ion-row style={{ height: '100vh' }} align-items-center justify-content-around>
            <ion-col>
              <img class="ion-padding center-horiz"
                   src="/assets/Sillibi-Logo.svg" alt="Sillibi Logo"/>
              <ion-button href="/login" class="ion-padding center-horiz button-margin half-width" color="secondary">
                Login
              </ion-button>
              <ion-label class="ion-padding ion-text-center center-horiz">Don't have an account?</ion-label>
              <ion-button href="/register" style={{'display': 'table'}} class="center-horiz" fill="clear">
                  <ion-label color="tertiary">Create an Account</ion-label>
              </ion-button>
            </ion-col>
          </ion-row>
        </ion-grid>
      </ion-content>
    ]
  }
}
