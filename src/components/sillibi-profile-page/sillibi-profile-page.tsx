import {Component, Element, h, State} from '@stencil/core';
import {AppRoot} from "../app-root/app-root";

@Component({
  tag: 'sillibi-profile-page',
  styleUrl: 'sillibi-profile-page.css'
})
export class SillibiProfilePage {

  @Element() el: HTMLElement;

  @State() userData: any;

  private modalController: HTMLIonModalControllerElement;

  async componentWillRender() {
    await AppRoot.getApiHelper().waitForUser();

    this.userData = {...AppRoot.getApiHelper().user.data};
  }

  async onLogoutClicked() {
    const result = await AppRoot.getApiHelper().signOut()
      .catch((err) => {
        AppRoot.showNotification(err, 'danger');
      });

    if (!result) return;

    await AppRoot.route('/login');
  }

  async onEditClicked() {
    const modal = await this.modalController.create({
      component: 'sillibi-profile-edit',
      componentProps: {
        modalController: this.modalController
      }
    });

    await modal.present();

    const {data} = await modal.onWillDismiss();
    if(!data) return;

    await this.componentWillRender();
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar>
          <ion-title>My Profile</ion-title>
          <ion-buttons slot="primary">
            <ion-button color="tertiary" onClick={() => this.onEditClicked()}>Edit</ion-button>
            <ion-button color="tertiary" onClick={() => this.onLogoutClicked()}>Logout</ion-button>
          </ion-buttons>
        </ion-toolbar>
      </ion-header>,
      <ion-modal-controller ref={(el) => this.modalController = el}/>,
      <ion-content color="light">
        <ion-card style={{'--background': '#ffffff'}}>
          <ion-card-content class="ion-no-padding">
            <div class="ion-padding-vertical">
              <ion-avatar class="center-horiz">
                <img src={this.userData.avatar || `https://ui-avatars.com/api/?name=
                            ${encodeURI(this.userData.first_name)}
                            + ${encodeURI(this.userData.last_name)}
                            &rounded=true&background=32134E&color=ffffff`}/>
              </ion-avatar>
            </div>

            <ion-list lines="none">
              <ion-item lines="none" class="ion-form-text ion-padding-horizontal">
                <ion-label color="medium" position="stacked">First Name</ion-label>
                <ion-input required type="text"
                           value={this.userData.first_name}
                           readonly={true}/>
              </ion-item>
              <ion-item lines="none" class="ion-form-text ion-padding-horizontal">
                <ion-label color="medium" position="stacked">Last Name</ion-label>
                <ion-input required type="text"
                           value={this.userData.last_name}
                           readonly={true}/>
              </ion-item>
              <ion-item lines="none" class="ion-form-text ion-padding-horizontal">
                <ion-label color="medium" position="stacked">Email address</ion-label>
                <ion-input required type="email"
                           value={this.userData.email}
                           readonly={true}/>
              </ion-item>

              <ion-item-divider class="ion-padding-top ion-text-uppercase">
                <ion-label class="ion-padding-horizontal">Communication Settings</ion-label>
              </ion-item-divider>
            </ion-list>

            {this.userData.calls_enabled ? (
              <ion-button class="ion-padding-start" fill="clear" disabled={true}>
                <ion-icon name="call"/>
              </ion-button>
            ) : null}
            {this.userData.emails_enabled ? (
              <ion-button class="ion-padding-start" fill="clear" disabled={true}>
                <ion-icon name="mail"/>
              </ion-button>
            ) : null}
          </ion-card-content>
        </ion-card>
      </ion-content>,
      <sillibi-tab-bar tab="profile"/>
    ]
  }
}
