import {Component, h, Prop} from '@stencil/core';
import {AppRoot} from "../app-root/app-root";

@Component({
  tag: 'sillibi-assignment-card',
  styleUrl: 'sillibi-assignment-card.css'
})
export class SillibiAssignmentCard {

  @Prop() course?: any;
  @Prop() assignments: any[];

  componentWillLoad() {
    this.assignments.sort((a, b) => {
      return a.possible_points < b.possible_points ? 1 : -1
    });
  }

  private async onCardClicked() {
    await AppRoot.route(this.course
      ? `/courses/${this.course.id}/assignments`
      : `/assignments/${this.assignments[0].id}`);
  }

  private generateEntries() {
    return this.assignments.map((assignment) => {
      return [
          <ion-item lines="none">
              <ion-label slot="start" class="ion-text-start">{assignment.name}</ion-label>
              <ion-label slot="end" class="ion-text-end">{assignment.possible_points} pts</ion-label>
          </ion-item>,
          <ion-item lines="full">
            <ion-label color="medium" text-wrap>{assignment.description}</ion-label>
          </ion-item>
      ]
    });
  }

  render() {
    return [
      <ion-card button={true} onClick={() => this.onCardClicked()} style={{'--background': '#ffffff'}}>
        <ion-card-content class="ion-no-padding">
          {this.course ? [
            <ion-item style={{'--border-color': this.course.color}} lines="full">
              <ion-label class="ion-form-text">{this.course.name}</ion-label>
            </ion-item>
          ] : null}

          {this.generateEntries()}
        </ion-card-content>
      </ion-card>
    ];
  }
}
