import {Component, Element, h, Prop, State} from '@stencil/core';
import {AppRoot} from "../app-root/app-root";
import {differenceInCalendarDays, formatDistance} from 'date-fns';
import _ from 'underscore';

@Component({
  tag: 'sillibi-assignments-page',
  styleUrl: 'sillibi-assignments-page.css'
})
export class SillibiAssignmentsPage {

  @Element() el: HTMLElement;

  @State() assignments: any[] = [];

  @Prop() courseId: any;

  private courses: any[] = [];
  private modalController: HTMLIonModalControllerElement;

  async componentWillRender() {
    await AppRoot.getApiHelper().waitForUser();

    let result = await AppRoot.getCourseHelper().requestAll(
      this.courseId ? `id=${this.courseId}` : null
    );
    if (!result.data) return;

    this.courses = result.data;

    result = await AppRoot.getAssignmentHelper().requestAll(
      this.courseId ? `course_id=${this.courseId}` : null
    );
    if (!result.data) return;

    this.assignments = result.data.sort((a, b) => {
      return a.possible_points < b.possible_points ? 1 : -1
    });
  }

  async onNewAssignmentClicked() {
    const modal = await this.modalController.create({
      component: 'sillibi-assignments-create',
      componentProps: {
        modalController: this.modalController,
        courseId: this.courseId
      }
    });

    await modal.present();

    const {data} = await modal.onWillDismiss();
    if (!data) return;

    this.assignments = [...this.assignments, data];
  }

  private generateCard(list, course = null, assignments) {
    const card = <sillibi-assignment-card course={course} assignments={assignments}/>;

    console.log('list:', list);

    const key = _.findKey(list, (val) => Array.isArray(val));
    list[key].push(card);
  }

  private generateAssignments() {
    const lists = {};

    this.assignments.forEach((assignment) => {
      const {due_date} = assignment;
      const date_today = new Date();
      const date_then = new Date(due_date);
      const key = date_then.toDateString();
      const day_diff = differenceInCalendarDays(date_then, date_today);

      lists[key] = lists[key] || (
        <ion-list style={{'background': 'var(--ion-color-light)'}}>
          <ion-list-header>
            <ion-label color={day_diff < 0 ? "danger" : day_diff === 0 ? "primary" : ""}
                       class={"bold-text"}>
              {day_diff === -1 ? "Yesterday (past-due)"
                : day_diff === 0 ? "Today"
                  : day_diff === 1 ? "Tomorrow"
                    : formatDistance(date_then, date_today, {addSuffix: true}) }
            </ion-label>
          </ion-list-header>
        </ion-list>
      );
    });

    if (this.courseId) {
      // show assignments for particular course
      this.assignments.forEach((assignment) => {
        const {due_date} = assignment;
        const date_then = new Date(due_date);
        const list = lists[date_then.toDateString()];

        this.generateCard(list, null, [assignment]);
      });
    } else {
      // show the two most pressing assignments for each course
      this.courses.forEach((course) => {
        const date_assignment_lists = {};

        this.assignments.forEach((assignment) => {
          if(assignment.course_id !== course.id) return;

          const {due_date} = assignment;
          const date_then = new Date(due_date);
          const key = date_then.toDateString();

          date_assignment_lists[key] = date_assignment_lists[key] || [];
          date_assignment_lists[key].push(assignment);
        });

        Object.keys(date_assignment_lists).forEach((dateStr) => {
          const assignment_list = date_assignment_lists[dateStr];
          const card_list = lists[dateStr];

          this.generateCard(card_list, course, assignment_list);
        });
      });
    }

    return Object.keys(lists)
      .sort((a, b) => { return (new Date(a) > new Date(b)) ? 1 : -1 })
      .map((dateStr) => lists[dateStr]);
  }

  private renderAssignments = () => [
    <ion-grid>
      <ion-row align-items-center justify-content-around>
        <ion-col>
          {this.generateAssignments()}
        </ion-col>
      </ion-row>
      {this.courseId ?
        (
          <ion-row>
            <ion-col>
              <ion-button class="center-horiz" fill="clear" shape="round"
                          onClick={() => this.onNewAssignmentClicked()}>
                <img src="/assets/BTN_AddAssignment.svg"/>
              </ion-button>
            </ion-col>
          </ion-row>
        ) : null}
    </ion-grid>
  ];

  private renderNoAssignments = () => (
    <ion-grid>
      <ion-row style={{height: '65vh'}} align-items-center justify-content-around>
        <ion-col>
          <img class="center-horiz" style={{height: '50vh', width: '50vh'}} src="/assets/Illo-robot.svg"/>
        </ion-col>
      </ion-row>
      <ion-row>
        <ion-col>
          <ion-label class="ion-padding center-horiz ion-text-center" text-wrap>
            Our robots are working hard uploading other assignments.
            {this.courseId
              ? ' Would you like to manually upload them?'
              : ' Visit your courses page for manual uploading.'}
          </ion-label>
          {this.courseId ?
            (
              <ion-button class="center-horiz" color="tertiary" fill="clear"
                          onClick={() => this.onNewAssignmentClicked()}>
                Input Manual Assignments
              </ion-button>
            ) : null}
        </ion-col>
      </ion-row>
    </ion-grid>
  );

  render() {
    return [
      <ion-header>
        <ion-toolbar>
          {this.courseId ? (
            <ion-buttons slot="start">
              <ion-back-button defaultHref="/courses"/>
            </ion-buttons>
          ) : null}
          <ion-title>Assignments</ion-title>
        </ion-toolbar>
      </ion-header>,
      <ion-modal-controller ref={(el) => this.modalController = el}/>,
      <ion-content color="light">
        {this.assignments.length > 0 ? this.renderAssignments() : this.renderNoAssignments()}
      </ion-content>,
      <sillibi-tab-bar hidden={this.courseId !== undefined} tab="assignments"/>
    ]
  }
}
