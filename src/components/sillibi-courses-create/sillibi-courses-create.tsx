import {Component, Element, h, Prop} from '@stencil/core';
import $ from "jquery";
import {AppRoot} from "../app-root/app-root";

@Component({
  tag: 'sillibi-courses-create',
  styleUrl: 'sillibi-courses-create.css'
})
export class SillibiCoursesCreate {

  @Element() el: HTMLElement;

  @Prop() modalController: HTMLIonModalControllerElement;

  private form: HTMLFormElement;
  private colorPicker: HTMLSillibiColorPickerElement;
  private debounce = false;

  async onCancelClicked() {
    await this.modalController.dismiss();
  }

  async onAddCourseClicked() {
    if(this.debounce || !this.form.reportValidity()) {
      return;
    }

    this.debounce = true;

    const name = $('#input-course-name').val().toString();
    const course_number = $('#input-course-number').val().toString();
    const section_number = $('#input-section-number').val().toString();
    const term = $('#input-term').val().toString();
    const instructor = $('#input-instructor').val().toString();
    const color = await this.colorPicker.getCurrentColor();

    const button = $('#button-add-course')[0] as HTMLIonButtonElement;
    button.disabled = true;

    const result = await AppRoot.getCourseHelper().create({
        name, course_number, section_number, term, instructor, color
      })
      .catch((err) => {
        AppRoot.showNotification(err, 'danger');
      });

    this.debounce = false;
    button.disabled = false;

    if(!result) return;

    return this.modalController.dismiss(result.data);
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar>
          <ion-title>Add Course</ion-title>
          <ion-buttons slot="primary">
            <ion-button color="tertiary" onClick={() => this.onCancelClicked()}>Cancel</ion-button>
          </ion-buttons>
        </ion-toolbar>
      </ion-header>,
      <ion-content color="light">
        <ion-card style={{'--background': '#ffffff'}}>
          <ion-card-content class="ion-no-padding">
            <form ref={(el) => this.form = el}>
              <ion-list lines="none">
                <ion-item lines="inset" class="ion-form-text ion-padding-horizontal">
                  <ion-label color="dark" position="stacked">Course name</ion-label>
                  <ion-input id="input-course-name" required type="text" color="medium"
                             placeholder="Business administration"/>
                </ion-item>
                <ion-item lines="inset" class="ion-form-text ion-padding-horizontal">
                  <ion-label color="dark" position="stacked">Course number</ion-label>
                  <ion-input id="input-course-number" required type="text" color="medium"
                             placeholder="e.g., MGMT 100"/>
                </ion-item>
                <ion-item lines="inset" class="ion-form-text ion-padding-horizontal">
                  <ion-label color="dark" position="stacked">Section number</ion-label>
                  <ion-input id="input-section-number" required type="text" color="medium"
                             placeholder="e.g., 001"/>
                </ion-item>
                <ion-item lines="inset" class="ion-form-text ion-padding-horizontal">
                  <ion-label color="dark" position="stacked">Term</ion-label>
                  <ion-input id="input-term" required type="text" color="medium"
                             placeholder="Fall 2017"/>
                </ion-item>
                <ion-item lines="inset" class="ion-form-text ion-padding-horizontal">
                  <ion-label color="dark" position="stacked">Instructor</ion-label>
                  <ion-input id="input-instructor" required type="text" color="medium"
                             placeholder="Dr. Sam Birk"/>
                </ion-item>

                <ion-item lines="none" class="ion-form-text ion-padding-horizontal">
                  <ion-label color="dark" position="stacked">Color</ion-label>
                </ion-item>
                <sillibi-color-picker ref={(el) => this.colorPicker = el} />
              </ion-list>
            </form>

            <ion-button id="button-add-course" class="ion-padding center-horiz half-width" color="secondary"
                        onClick={() => this.onAddCourseClicked()}>Add Course</ion-button>
          </ion-card-content>
        </ion-card>
      </ion-content>
    ]
  }
}
