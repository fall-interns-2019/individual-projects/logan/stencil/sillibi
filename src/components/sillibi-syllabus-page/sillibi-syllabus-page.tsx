import {Component, Element, h, Prop, State} from '@stencil/core';
import {AppRoot} from "../app-root/app-root";
import {getBase64FromFile} from "../../helpers/utils";

@Component({
  tag: 'sillibi-syllabus-page',
  styleUrl: 'sillibi-syllabus-page.css'
})
export class SillibiSyllabusPage {

  @Element() el: HTMLElement;

  @State() syllabuses: any[] = [];

  @Prop() courseId: number;

  private alertController: HTMLIonAlertControllerElement;
  private debounce = false;

  async componentWillRender() {
    await AppRoot.getApiHelper().waitForUser();

    const {data} = await AppRoot.getSyllabusHelper().requestAll(
      `course_id=${this.courseId}`
    );
    if (!data) return;

    this.syllabuses = data;
  }

  private async onFileButtonChanged(event) {
    if (!event.target || event.target.files.length === 0) return;

    const pdf = event.target.files[0] as File;
    const encoded = await getBase64FromFile(pdf);

    try {
      const result = await AppRoot.getSyllabusHelper().create({image: encoded, course_id: this.courseId}) as any;
      this.syllabuses = [...this.syllabuses, result]
    } catch (err) {
      await AppRoot.showNotification(err, 'danger');
    }
  }

  private onUploadClicked() {
    const file_button = this.el.querySelector('#file-button') as HTMLButtonElement;
    file_button.click();
  }

  private async onDeleteClicked() {
    if (this.debounce) return;

    this.debounce = true;

    const alert = await this.alertController.create({
      header: 'Confirmation',
      message: 'Are you sure you want to <strong>delete</strong> this syllabus?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Delete',
          role: 'delete'
        }
      ]
    });

    await alert.present();
    const result = await alert.onWillDismiss();

    if (result.role !== 'delete') {
      this.debounce = false;
      return;
    }

    const delete_result = await AppRoot.getSyllabusHelper().destroy(this.syllabuses[0].id)
      .catch((err) => {
        AppRoot.showNotification(err, 'danger');
      });

    this.debounce = false;

    if (!delete_result) return;

    await AppRoot.route('/courses');
  }

  private generateImages() {
    return this.syllabuses.map((syllabus) => {
      return (
        <ion-card style={{'--background': '#ffffff', height: '100%'}} class="ion-padding">
          <iframe src={syllabus.image} style={{width: '100%', height: '100%'}}/>
        </ion-card>
      )
    });
  }

  private renderPictures() {
    return [
      <ion-content color="light">
        <ion-list style={{'background': 'var(--ion-color-light)', height: '100%'}} class="ion-padding">
          {this.generateImages()}
        </ion-list>
      </ion-content>
    ]
  };


  private renderNoPictures() {
    return [
      <ion-content color="light">
        <ion-grid>
          <ion-row style={{height: '75vh'}} align-items-center justify-content-around>
            <ion-col>
              <img style={{height: '50vh', width: '50vh'}} class="center-horiz" src="/assets/Illo-robot.svg"/>
            </ion-col>
          </ion-row>
          <ion-row>
            <ion-col>
              <ion-label class="ion-padding center-horiz" text-wrap>
                Upload your syllabus and one of our robot elves will input your assignments for you.
              </ion-label>
            </ion-col>
          </ion-row>
          <ion-row>
            <ion-col>
              <input id="file-button" type="file" accept="application/pdf" hidden
                     onChange={(evt) => this.onFileButtonChanged(evt)}/>
              <ion-button class="center-horiz" color="tertiary" fill="clear"
                          onClick={() => this.onUploadClicked()}>Upload PDF
              </ion-button>
            </ion-col>
          </ion-row>
        </ion-grid>
      </ion-content>
    ]
  };

  render() {
    return [
      <ion-header>
        <ion-toolbar>
          <ion-buttons slot="start">
            <ion-back-button defaultHref="/courses"/>
          </ion-buttons>
          <ion-title>Syllabus</ion-title>
          {this.syllabuses.length > 0 ? (
            <ion-buttons slot="primary">
              <ion-button color="danger" onClick={() => this.onDeleteClicked()}>Delete</ion-button>
            </ion-buttons>
          ) : null}
        </ion-toolbar>
      </ion-header>,
      <ion-alert-controller ref={(el) => this.alertController = el}/>,
      this.syllabuses.length > 0 ? this.renderPictures() : this.renderNoPictures()
    ]
  }
}
