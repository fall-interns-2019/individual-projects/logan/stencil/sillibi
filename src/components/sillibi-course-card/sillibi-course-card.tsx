import {Component, Element, Event, EventEmitter, h, Prop} from '@stencil/core';
import {AppRoot} from "../app-root/app-root";

@Component({
  tag: 'sillibi-course-card',
  styleUrl: 'sillibi-course-card.css'
})
export class SillibiCourseCard {

  @Element() el: HTMLElement;

  @Prop() course: any;

  @Event() assignmentCreated: EventEmitter;

  private modalController: HTMLIonModalControllerElement;

  componentWillLoad() {
    this.modalController =
      document.querySelector('ion-modal-controller') as HTMLIonModalControllerElement;
  }

  private async viewCourse() {
    await AppRoot.route(`/courses/${this.course.id}`);
  }

  private async viewSyllabuses() {
    await AppRoot.route(`/courses/${this.course.id}/syllabus`);
  }

  private async viewAssignments() {
    await AppRoot.route(`/courses/${this.course.id}/assignments`);
  }

  private async onCreateAssignmentClicked(event) {
    event.stopPropagation();

    const modal = await this.modalController.create({
      component: 'sillibi-assignments-create',
      componentProps: {
        modalController: this.modalController,
        courseId: this.course.id
      }
    });

    await modal.present();

    const {data} = await modal.onWillDismiss();
    if (!data) return;

    this.assignmentCreated.emit(data);
  }

  render() {
    return [
      <ion-card style={{'--background': '#ffffff'}}>
        <ion-card-content class="ion-no-padding">
          <ion-item button={true} onClick={() => this.viewCourse()}
                    style={{'--border-color': this.course.color}} lines="full">
            <ion-label class="ion-form-text">{this.course.name}</ion-label>
          </ion-item>
          <ion-item button={true} onClick={() => this.viewCourse()} lines="full">
            <ion-label color="medium" text-wrap>
              {this.course.course_number} - {this.course.section_number}
              <br/>
              {this.course.instructor}
            </ion-label>
          </ion-item>
          <ion-item button={true} onClick={() => this.viewSyllabuses()} lines="full">
            <ion-icon name="document" slot="start" color="tertiary"/>
            <ion-label>Syllabus</ion-label>
            <ion-button fill="clear" shape="round" slot="end">
              <ion-icon name={this.course.syllabus_count > 0 ? "arrow-forward" : "add-circle-outline"}
                        slot="icon-only" color="tertiary"/>
            </ion-button>
          </ion-item>
          <ion-item button={true} onClick={() => this.viewAssignments()} lines="full">
            <ion-icon name="list-box" slot="start" color="tertiary"/>
            <ion-label>Assignments</ion-label>
            <ion-note slot="end">{this.course.assignment_count}</ion-note>
            <ion-button fill="clear" shape="round" slot="end"
                        onClick={(evt) => this.onCreateAssignmentClicked(evt)}>
              <ion-icon name="add-circle-outline" slot="icon-only" color="tertiary"/>
            </ion-button>
          </ion-item>
        </ion-card-content>
      </ion-card>
    ];
  }
}
