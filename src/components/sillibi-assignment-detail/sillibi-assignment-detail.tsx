import {Component, Element, h, Prop, State} from '@stencil/core';
import {AppRoot} from "../app-root/app-root";
import $ from 'jquery';

@Component({
  tag: 'sillibi-assignment-detail',
  styleUrl: 'sillibi-assignment-detail.css'
})
export class SillibiAssignmentDetail {

  @Element() el: HTMLElement;

  @Prop() assignmentId: number;

  @State() editMode = false;

  private assignment: any;
  private courseId: number;
  private debounce = false;

  private alertController: HTMLIonAlertControllerElement;
  private form: HTMLFormElement;

  async componentWillLoad() {
    const {data} = await AppRoot.getAssignmentHelper().get(this.assignmentId);
    if(!data) return;

    this.assignment = data;
    this.courseId = data.course_id;
  }

  onEditButtonClick() {
    this.editMode = !this.editMode;
  }

  async onSaveButtonClicked() {
    if(this.debounce || !this.form.reportValidity()) {
      return;
    }

    this.debounce = true;

    const name = $('#input-assignment-name').val().toString();
    const due_date = $('#input-due-date').val().toString();
    const description = $('#input-description').val().toString();
    const possible_points = $('#input-possible-points').val().toString();

    const button = $('#button-save')[0] as HTMLIonButtonElement;
    button.disabled = true;

    const result = await AppRoot.getAssignmentHelper().update(this.assignmentId, {
      name, due_date, description, possible_points
    })
      .catch((err) => {
        AppRoot.showNotification(err, 'danger');
      });

    this.debounce = false;
    button.disabled = false;

    if(!result) return;

    this.editMode = false;
  }

  async onDeleteButtonClicked() {
    if(this.debounce) return;

    this.debounce = true;

    const alert = await this.alertController.create({
      header: 'Confirmation',
      message: 'Are you sure you want to <strong>delete</strong> this assignment?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Delete',
          role: 'delete'
        }
      ]
    });

    await alert.present();
    const result = await alert.onWillDismiss();

    if(result.role !== 'delete') {
      this.debounce = false;
      return;
    }

    const delete_result = await AppRoot.getAssignmentHelper().destroy(this.assignmentId)
      .catch((err) => {
        AppRoot.showNotification(err, 'danger');
      });

    this.debounce = false;

    if(!delete_result) return;

    await AppRoot.route(`/courses/${this.courseId}/assignments`);
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar>
          <ion-buttons slot="start">
            <ion-back-button defaultHref={`/courses/${this.courseId}/assignments`} />
          </ion-buttons>
          <ion-title>{this.editMode ? 'Edit Assignment' : 'Assignment Information'}</ion-title>
          <ion-buttons slot="primary">
            <ion-button color="tertiary"
                        onClick={() => this.onEditButtonClick()}>
              {this.editMode ? 'Cancel' : 'Edit'}
            </ion-button>
          </ion-buttons>
        </ion-toolbar>
      </ion-header>,
      <ion-alert-controller ref={(el) => this.alertController = el} />,
      <ion-content color="light">
        <ion-card style={{'--background': '#ffffff'}}>
          <ion-card-content class="ion-no-padding">
            <form ref={(el) => this.form = el}>
              <ion-list lines="none">
                <ion-item lines={this.editMode ? "inset" : "none"} class="ion-form-text ion-padding-horizontal">
                  <ion-label color="medium" position="stacked">Assignment name</ion-label>
                  <ion-input id="input-assignment-name" required type="text" color="dark"
                             value={this.assignment.name} readonly={!this.editMode}/>
                </ion-item>
                <ion-item lines={this.editMode ? "inset" : "none"} class="ion-form-text ion-padding-horizontal">
                  <ion-label color="medium" position="stacked">Due date</ion-label>
                  <ion-datetime id="input-due-date" display-format="MM/DD/YYYY" color="dark"
                                value={this.assignment.due_date} readonly={!this.editMode}
                                min={(new Date().getFullYear() - 1).toString()}
                                max={(new Date().getFullYear() + 1).toString()}/>
                </ion-item>
                <ion-item lines={this.editMode ? "inset" : "none"} class="ion-form-text ion-padding-horizontal">
                  <ion-label color="medium" position="stacked">Description</ion-label>
                  <ion-input id="input-description" required type="text" color="dark"
                             value={this.assignment.description} readonly={!this.editMode}/>
                </ion-item>
                <ion-item lines={this.editMode ? "inset" : "none"} class="ion-form-text ion-padding-horizontal">
                  <ion-label color="medium" position="stacked">Possible points</ion-label>
                  <ion-input id="input-possible-points" required type="number"
                             min="0" max="1000" color="dark"
                             value={this.assignment.possible_points} readonly={!this.editMode}/>
                </ion-item>
              </ion-list>
            </form>

            {this.editMode ?
              [
                <br/>,
                <ion-button id="button-save" class="center-horiz half-width" color="secondary"
                            onClick={() => this.onSaveButtonClicked()}>
                  Save Changes
                </ion-button>,
                <br/>,
                <ion-button id="button-delete" class="center-horiz half-width" color="medium" fill="clear"
                            onClick={() => this.onDeleteButtonClicked()}>
                  Delete Assignment
                </ion-button>,
                <br />
              ] : null
            }
          </ion-card-content>
        </ion-card>
      </ion-content>
    ]
  }
}
