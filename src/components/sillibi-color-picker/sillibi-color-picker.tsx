import {Component, h, Method, State} from "@stencil/core";

@Component({
  tag: 'sillibi-color-picker',
  styleUrl: 'sillibi-color-picker.css'
})
export class SillibiColorPicker {
  @State() currentColor: string;

  colors = [
    ['#ffce00', '#f04141', '#3880ff', '#0bb8cc', '#989aa2'],
    ['#f25454', '#f04100', '#B9B8FF', '#24d6ea', '#86888f'],
    ['#d33939', '#B800B5', '#9757FF', '#0ec254', '#005C42'],
    ['#FF6200', '#80006F', '#DA0AFF', '#28e070', '#006B2E'],
  ];

  componentWillLoad() {
    this.currentColor = this.colors[0][0];
  }

  @Method()
  async getCurrentColor() {
    return this.currentColor;
  }

  private generateColorRows() {
    return this.colors.map((rowArray) => {
      return (
        <ion-row class="ion-padding" align-content-center justify-content-around>
          {rowArray.map((hex) => {
            return (
              <ion-col>
                <div
                  class={"color-circle-outer center-horiz " + (this.currentColor === hex ? "color-circle-selected" : "")}>
                  <div class="color-circle-inner" style={{background: hex}} onClick={() => this.currentColor = hex}/>
                </div>
              </ion-col>
            )
          })}
        </ion-row>
      )
    })
  }

  render() {
    return (
      <ion-grid class="ion-padding-vertical">
        {this.generateColorRows()}
      </ion-grid>
    )
  }
}
