import {RestHelper} from "./restHelper";

export class CourseHelper extends RestHelper {
  constructor() {
    super('courses');
  }
}
