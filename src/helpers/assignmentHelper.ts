import {RestHelper} from "./restHelper";

export class AssignmentHelper extends RestHelper {
  constructor() {
    super('assignments');
  }
}
