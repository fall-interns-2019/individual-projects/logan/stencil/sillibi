import {RestHelper} from "./restHelper";

export class SyllabusHelper extends RestHelper {
  constructor() {
    super('syllabuses');
  }
}
